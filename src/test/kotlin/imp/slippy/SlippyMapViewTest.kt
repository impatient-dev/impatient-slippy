package imp.slippy

import org.junit.Assert.assertEquals
import org.junit.Test

private const val dim = 256

class SlippyMapViewTest {
	@Test fun test_upperLeftZoom() {
		val w = dim * 4 - dim/3
		val h = dim * 3 - dim/3
		val z0 = SlippyMapView(
			TilePos(0, 0, 0),
			TileRegion(0, 0, 0, 0, 0),
			0, 0, w, h
		)
		val z1 = z0.zoomIn(dim)
		assertEquals(SlippyMapView(
			TilePos(1, 0, 0),
			TileRegion(1, 0, 0, 1, 1),
			0, 0, w, h
		), z1)
		val z2 = z1.zoomIn(dim)
		assertEquals(SlippyMapView(
			TilePos(2, 0, 0),
			TileRegion(2, 0, 0, 1, 1),
			0, 0, w, h
		), z2)
		val z3 = z2.zoomIn(dim)
		assertEquals(SlippyMapView(
			TilePos(3, 0, 0),
			TileRegion(3, 0, 0, 1, 1),
			0, 0, w, h
		), z3)
	}


	@Test fun test_centerZoom() {
		val w = dim * 4 - dim/3
		val h = dim * 3 - dim/3
		val z0 = SlippyMapView(
			TilePos(0, 0, 0),
			TileRegion(0, 0, 0, 0, 0),
			dim/2, dim/2, w, h
		)
		val z1 = z0.zoomIn(dim)
		assertEquals(SlippyMapView(
			TilePos(1, 1, 1),
			TileRegion(1, 0, 0, 1, 1),
			0, 0, w, h
		), z1)
	}
}