package imp.slippy

import kotlin.math.abs
import kotlin.math.roundToLong

private val hague1 = Pair(52.081708, 4.322882)
private val hague2 = Pair(52.081703, 4.322871)
private val hagueIntersection = Pair(52.081963, 4.321777)
private val rotterdam = Pair( 51.923434, 4.470306)
private val wellington = Pair(-41.279638, 174.779891)
private val oppositeHague = Pair<Double, Double>(-37.918292, -175.677118)


fun main() {
	test(0.94, hague1, hague2)
	test(79.9, hague1, hagueIntersection)
	test(20_376.05, hagueIntersection, rotterdam)
	test(18_605_414.84, hagueIntersection, wellington)
	test(18_797_571.78, hagueIntersection, oppositeHague)

}

private fun test(expected: Double, a: Pair<Double, Double>, b: Pair<Double, Double>) {
	println("Expect: $expected")
	printActual(expected, a, b, LawCosinesSDC, "LoC")
	printActual(expected, a, b, HaversineSDC, "Hav")
	printActual(expected, a, b, VicentySDC, "Vic")
}

private fun printActual(expected: Double, a: Pair<Double, Double>, b: Pair<Double, Double>, sdc: SphereDistanceCalculator, name: String) {
	val actual = sdc.degrees(a.first, a.second, b.first, b.second)
	val error = abs(actual - expected) / expected
	println("\t$name $actual  error ${(error * 100).roundToLong()}%")
}