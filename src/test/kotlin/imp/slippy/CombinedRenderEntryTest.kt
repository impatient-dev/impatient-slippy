package imp.slippy

import org.junit.Assert.assertEquals
import org.junit.Test

class CombinedRenderEntryTest {
	@Test fun test_ImagePixels_roundTrip() {
		val dim = 16
		val cases = arrayOf(
			RightBottom(false, false),
			RightBottom(true, false),
			RightBottom(false, true),
			RightBottom(true, true),
		)
		for(case in cases) {
			println("Test case: $case")
			val p: ImgPixels = ImgPixels.quarter(null, dim, case.right, case.bottom)
			assertEquals(if(case.right) 8 else 0, p.x0)
			assertEquals(if(case.bottom) 8 else 0, p.y0)
			assertEquals(if(case.right) 15 else 7, p.x1)
			assertEquals(if(case.bottom) 15 else 7, p.y1)

			assertEquals(null, p.quadruple(dim))
		}

	}
}


private data class RightBottom(
	val right: Boolean,
	val bottom: Boolean,
)