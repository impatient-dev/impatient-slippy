package imp.slippy

import imp.junit4.assertSetsEqual
import imp.logback.initLogbackTrace
import org.junit.Test

private val settings = SlippySettings(PlateCarreeProjection, 16, 1, 1)

private inline fun cre(zoom: Int, x: Int, y: Int, image: String? = null, imagePos: TilePos = TilePos(zoom, x, y))
	= CombinedRenderEntry<String>(image, TilePos(zoom, x, y), imagePos, null, null)

class CombinedRenderHolderTest {
	private var entries: List<CombinedRenderEntry<String>> = emptyList()

	init { initLogbackTrace() }

	private fun assertEntries(vararg expect: CombinedRenderEntry<String>) = assertSetsEqual(expect.asList(), entries)


	/**Test where there are never any images.*/
	@Test fun test_empty() {
		val holder = CombinedRenderHolder<String>(TileRegion(0, 0, 0, 0, 0), settings) { entries = it }
		assertEntries(cre(0, 0, 0))

		holder.setRegion(TileRegion(1, 0, 0, 1, 1))
		assertEntries(cre(1, 0, 0), cre(1, 0, 1), cre(1, 1, 0), cre(1, 1, 1))

		holder.setRegion(TileRegion(2, 2, 2, 2, 3))
		assertEntries(cre(2, 2, 2), cre(2, 2, 3))

		holder.setRegion(TileRegion(1, 1, 1, 1, 1))
		assertEntries(cre(1, 1, 1))
	}


	/**Test with all images filled.*/
	@Test fun test_full() {
		val holder = CombinedRenderHolder<String>(TileRegion(0, 0, 0, 0, 0), settings) { entries = it }
		assertEntries(cre(0, 0, 0))
		holder.putRender(TilePos(0, 0, 0), "0")
		assertEntries(cre(0, 0, 0, "0"))

		holder.setRegion(TileRegion(1, 0, 0, 1, 1))
		assertEntries(
			cre(1, 0, 0), cre(1, 0, 1), cre(1, 1, 0), cre(1, 1, 1),
			cre(1, 0, 0, "0", TilePos(0, 0, 0)),
			cre(1, 0, 1, "0", TilePos(0, 0, 0)),
			cre(1, 1, 0, "0", TilePos(0, 0, 0)),
			cre(1, 1, 1, "0", TilePos(0, 0, 0)),
		)
		holder.putRender(TilePos(1, 1, 1), "111")
		assertEntries(
			cre(1, 0, 0), cre(1, 0, 1), cre(1, 1, 0), cre(1, 1, 1, "111"),
			cre(1, 0, 0, "0", TilePos(0, 0, 0)),
			cre(1, 0, 1, "0", TilePos(0, 0, 0)),
			cre(1, 1, 0, "0", TilePos(0, 0, 0)),
		)
		holder.putRender(TilePos(1, 0, 0), "100")
		assertEntries(
			cre(1, 0, 0, "100"), cre(1, 0, 1), cre(1, 1, 0), cre(1, 1, 1, "111"),
			cre(1, 0, 1, "0", TilePos(0, 0, 0)),
			cre(1, 1, 0, "0", TilePos(0, 0, 0)),
		)
		holder.putRender(TilePos(1, 0, 1), "101")
		assertEntries(
			cre(1, 0, 0, "100"), cre(1, 0, 1, "101"), cre(1, 1, 0), cre(1, 1, 1, "111"),
			cre(1, 1, 0, "0", TilePos(0, 0, 0)),
		)
		holder.putRender(TilePos(1, 1, 0), "110")
		assertEntries(
			cre(1, 0, 0, "100"), cre(1, 0, 1, "101"), cre(1, 1, 0, "110"), cre(1, 1, 1, "111"),
		)

		holder.setRegion(TileRegion(2, 2, 2, 2, 3))
		assertEntries(
			cre(2, 2, 2), cre(2, 2, 3),
			cre(2, 2, 2, "111", TilePos(1, 1, 1)),
			cre(2, 2, 3, "111", TilePos(1, 1, 1)),
		)
		holder.putRender(TilePos(2, 2, 2), "222")
		assertEntries(
			cre(2, 2, 2, "222"), cre(2, 2, 3),
			cre(2, 2, 3, "111", TilePos(1, 1, 1)),
		)
		holder.putRender(TilePos(2, 2, 3), "223")
		assertEntries(
			cre(2, 2, 2, "222"), cre(2, 2, 3, "223"),
		)

		holder.setRegion(TileRegion(1, 1, 1, 1, 1))
		assertEntries(cre(1, 1, 1), cre(1, 1, 1, "222", TilePos(2, 2, 2)), cre(1, 1, 1, "223", TilePos(2, 2, 3)))
		holder.putRender(TilePos(2, 0, 0), "wrong zoom - ignored")
		assertEntries(cre(1, 1, 1), cre(1, 1, 1, "222", TilePos(2, 2, 2)), cre(1, 1, 1, "223", TilePos(2, 2, 3)))
		holder.putRender(TilePos(1, 1, 1), "111")
		assertEntries(cre(1, 1, 1, "111"))
		holder.putRender(TilePos(1, 0, 0), "outside the desired region - ignored")
		assertEntries(cre(1, 1, 1, "111"))
	}


	/**Test where we zoom so far that the images are discarded.*/
	@Test fun test_discard() {
		val holder = CombinedRenderHolder<String>(TileRegion(0, 0, 0, 0, 0), settings) { entries = it }
		holder.putRender(TilePos(0, 0, 0), "foo")
		assertEntries(cre(0, 0, 0, "foo"))

		holder.setRegion(TileRegion(30, 0, 0, 1, 1))
		assertEntries(cre(30, 0, 0), cre(30, 0, 1), cre(30, 1, 0), cre(30, 1, 1))

		holder.putRender(TilePos(30, 0, 0), "foo")
		holder.putRender(TilePos(30, 0, 1), "bar")
		assertEntries(cre(30, 0, 0, "foo"), cre(30, 0, 1, "bar"), cre(30, 1, 0), cre(30, 1, 1))

		holder.setRegion(TileRegion(0, 0, 0, 0, 0))
		assertEntries(cre(0, 0, 0))
	}


	/**Reproduces a bug found by panning the map with the mouse.*/
	@Test fun test_manualPanIssueVertical() {
		val holder = CombinedRenderHolder<String>(TileRegion(2, 0, 1, 3, 2), settings) { entries = it }
		holder.setRegion(TileRegion(2, 0, 0, 3, 2))
		assertEntries(
			cre(2, 0, 0), cre(2, 0, 1), cre(2, 0, 2),
			cre(2, 1, 0), cre(2, 1, 1), cre(2, 1, 2),
			cre(2, 2, 0), cre(2, 2, 1), cre(2, 2, 2),
			cre(2, 3, 0), cre(2, 3, 1), cre(2, 3, 2),
		)
	}
	/**Reproduces a bug found by panning the map with the mouse.*/
	@Test fun test_manualPanIssueHorizontal() {
		val holder = CombinedRenderHolder<String>(TileRegion(1, 0, 0, 0, 1), settings) { entries = it }
		holder.setRegion(TileRegion(1, 0, 0, 1, 1))
		assertEntries(
			cre(1, 0, 0), cre(1, 0, 1), cre(1, 1, 0), cre(1, 1, 1)
		)
	}
}