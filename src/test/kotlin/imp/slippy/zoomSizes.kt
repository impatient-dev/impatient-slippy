package imp.slippy

import kotlin.math.roundToLong


// print approx tile size at different zoom levels
fun main() {
	val p = PlateCarreeProjection
	val dist = LawCosinesSDC
	println("Length of the diagonal of 1 tile, followed by sqrt(number of tiles present):")

	for(zoom in 0 until 30) {
		val m = maxTileIndex(zoom)
		val pos = TilePos(zoom, m / 2, m / 4) // northern hemisphere, moderate latitude
		val nw = pos.nwll(p)
		val se = pos.sell(p)
		println(
			"z" + "$zoom".padStart(2) + "  " +
			"${dist.degrees(nw.lat, nw.lon, se.lat, se.lon).roundToLong()}".padStart(8) + " m  " +
			"${tilesWH(zoom)}".padStart(8)
		)
	}
}