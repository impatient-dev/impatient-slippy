package imp.slippy

import org.junit.Assert.assertEquals
import org.junit.Test


class TestSlippyCore {
	@Test fun maxIndices() {
		assertEquals(0, maxTileIndex(0))
		assertEquals(1, maxTileIndex(1))
		assertEquals(3, maxTileIndex(2))
		assertEquals(7, maxTileIndex(3))
	}

	@Test fun posBasics() {
		assertEquals(TilePos(0, 0, 0), TilePos(1,0, 0).above())
		assertEquals(TilePos(0, 0, 0), TilePos(1,1, 1).above())
	}


	@Test fun regionContains() {
		assertEquals(true, TileRegion(2, 0, 0, 0, 0).contains(TilePos(2, 0, 0)))
		assertEquals(true, TileRegion(2, 0, 0, 1, 1).contains(TilePos(2, 0, 1)))
		// wrong x
		assertEquals(false, TileRegion(2, 1, 0, 1, 1).contains(TilePos(2, 0, 1)))
		// wrong y
		assertEquals(false, TileRegion(2, 0, 1, 0, 1).contains(TilePos(2, 0, 0)))
	}

	@Test fun regionUp() {
		assertEquals(TileRegion(0, 0, 0, 0, 0), TileRegion(1, 0, 0, 1, 1).above())
		assertEquals(TileRegion(0, 0, 0, 0, 0), TileRegion(1, 0, 0, 0, 0).above())
	}

	@Test fun regionDown() {
		assertEquals(TileRegion(1, 0, 0, 1, 1), TileRegion(0, 0, 0, 0, 0).below())
	}

	@Test fun iterator() {
		assertEquals(setOf(TilePos(5, 0, 0)), TileRegion(5, 0, 0, 0, 0).collect())
		assertEquals(setOf(
			TilePos(1, 0, 0),
			TilePos(1, 0, 1),
			TilePos(1, 1, 0),
			TilePos(1, 1, 1)
		), TileRegion(1, 0, 0, 1, 1).collect())
		assertEquals(setOf(
			TilePos(4, 1, 0),
			TilePos(4, 1, 1),
			TilePos(4, 1, 2),
			TilePos(4, 2, 0),
			TilePos(4, 2, 1),
			TilePos(4, 2, 2)
		), TileRegion(4, 1, 0, 2, 2).collect())
	}
}


private fun TileRegion.collect(): Set<TilePos> {
	val out = HashSet<TilePos>()
	forEachPos {out.add(it)}
	return out
}