package imp.slippy;

import org.junit.Test;

import static imp.slippy.WorldCoords.*;

import static org.junit.Assert.*;

public class WorldCoordsTest
{
	private static final double TOLERANCE = 0.0000001;


	@Test public void tileLeft_basic(){assertEquals(-180, tileLeft(0, 0, 0), TOLERANCE);}
	@Test public void tileLeft_out(){assertEquals(180, tileLeft(0, 1, 0), TOLERANCE);}
	@Test public void tileLeft_2(){assertEquals(-90, tileLeft(2, 1, 1), TOLERANCE);}


	@Test public void tileTop_basic(){assertEquals(90, tileTop(0, 0, 0), TOLERANCE);}
	@Test public void tileTop_out(){assertEquals(-90, tileTop(0, 0, 1), TOLERANCE);}
	@Test public void tileTop_2(){assertEquals(45, tileTop(2, 1, 1), TOLERANCE);}

	
	@Test public void tilePixel_0(){assertEquals(0, tilePixel(0, 0, 5));}
	@Test public void tilePixel_1(){assertEquals(Tiles.dim, tilePixel(5, 0, 5));}
	@Test public void tilePixel_0x(){assertEquals(0, tilePixel(2, 2, 2.5));}
	@Test public void tilePixel_x(){assertEquals(Tiles.dim * 3 / 5, tilePixel(2.3, 2, 2.5));}


	@Test public void fullTileX_basic(){assertEquals(0, fullTileX(0, 180, -180), TOLERANCE);}
	@Test public void fullTileX_center(){assertEquals(0.5, fullTileX(0, 0, 0), TOLERANCE);}
	@Test public void fullTileX_2(){assertEquals(2.5, fullTileX(2, -45, 45), TOLERANCE);}

	@Test public void fullTileY_basic(){assertEquals(0, fullTileY(0, 90, -90), TOLERANCE);}
	@Test public void fullTileY_center(){assertEquals(0.5, fullTileY(0, 0, 0), TOLERANCE);}
	@Test public void fullTileY_2(){assertEquals(2.5, fullTileY(2, -45.0/2, 45), TOLERANCE);}


	@Test public void tileX_main(){assertEquals(2, tileX(2, -45, 45));}

	@Test public void tileY_main(){assertEquals(2, tileY(2, -45.0/2, 45));}


	@Test public void pixelWidthLon_basic(){assertEquals(360.0 / Tiles.dim, pixelWidthLon(0), TOLERANCE);}
	@Test public void pixelWidthLon_1(){assertEquals(360.0 / Tiles.dim / 2, pixelWidthLon(1), TOLERANCE);}
	@Test public void pixelWidthLon_2(){assertEquals(360.0 / Tiles.dim / 4, pixelWidthLon(2), TOLERANCE);}

	@Test public void pixelHeightLat_basic(){assertEquals(180.0 / Tiles.dim, pixelHeightLat(0), TOLERANCE);}
	@Test public void pixelHeightLat_1(){assertEquals(180.0 / Tiles.dim / 2, pixelHeightLat(1), TOLERANCE);}
	@Test public void pixelHeightLat_2(){assertEquals(180.0 / Tiles.dim / 4, pixelHeightLat(2), TOLERANCE);}


	@Test public void latFor_basic(){assertEquals(90, latFor(0, 0), TOLERANCE);}
	@Test public void latFor_mid(){assertEquals(0, latFor(0, 0.5), TOLERANCE);}
	@Test public void latFor_2(){assertEquals(-45, latFor(1, 1.5), TOLERANCE);}

	@Test public void lonFor_basic(){assertEquals(-180, lonFor(0, 0), TOLERANCE);}
	@Test public void lonFor_mid(){assertEquals(0, lonFor(0, 0.5), TOLERANCE);}
	@Test public void lonFor_2(){assertEquals(-45, lonFor(2, 1.5), TOLERANCE);}
}