package imp.slippy;

import impatience.MathUtil;

import java.awt.image.BufferedImage;

/**Utility functions for tiles.
 * Tiles follow the Google Maps / OSM conventions:
 * -tiles are 256 pixels square
 * -at zoom level 0, the entire world fits on 1 tile; each level quadruples the number of tiles
 * -x ranges from 0 (west) to 2^zoom - 1 (east)
 * -y ranges from 0 (north) to 2^zoom - 1 (south)*/
@Deprecated public class Tiles
{
	/**The length and width of any tile image.*/
	public static final int dim = 256;
	/**Returns the maximum tile index (x or y) for a given zoom level: 2^zoom - 1*/
	public static int maxIndex(int zoom){return MathUtil.pow2(zoom) - 1;}
}
