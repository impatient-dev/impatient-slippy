package imp.slippy;

/**Utility functions relating to latitude/longitude and tiles.*/
public class WorldCoords
{
	/**Returns the longitude of the left edge of a tile.*/
	public static double tileLeft(int zoom, int x, int y)
	{
		return -180.0 + 360.0 * x / (SlippyCoreKt.maxTileIndex(zoom) + 1);
	}

	/**Returns the latitude of the top edge of a tile.*/
	public static double tileTop(int zoom, int x, int y)
	{
		return 90.0 - 180.0 * y / (SlippyCoreKt.maxTileIndex(zoom) + 1);
	}


	/**Chooses an int in [0, Tiles.dim] based on where input is in the range [min, max].*/
	public static int tilePixel(double input, double min, double max)
	{
		input -= min;
		max -= min;
		return (int)(input / max * Tiles.dim);
	}


	/**Returns the x-coordinate of the tile that contains this longitude.
	 * 0.3 means it's 30% of the way through the first tile.*/
	public static double fullTileX(int zoom, double lat, double lon)
	{
		return (180 + lon) / 360 * (Tiles.maxIndex(zoom) + 1);
	}
	/**Returns the y-coordinate of the tile that contains this latitude.
	 * 0.3 means it's 30% of the way through the first tile.*/
	public static double fullTileY(int zoom, double lat, double lon)
	{
		return (90 - lat) / 180 * (Tiles.maxIndex(zoom) + 1);
	}

	/**Returns the x-coordinate of the tile that contains this longitude.*/
	public static int tileX(int zoom, double lat, double lon)
	{
		return (int)Math.floor(fullTileX(zoom, lat, lon));
	}
	/**Returns the y-coordinate of the tile that contains this latitude.*/
	public static int tileY(int zoom, double lat, double lon)
	{
		return (int)Math.floor(fullTileY(zoom, lat, lon));
	}



	/**Returns a zoom and tile-position that will allow a window with pixelWidth*pixelHeight pixels to display the provided region.
	 * @param multiplier 1 means "return a region that just barely fits this"; larger adds wiggle room*/
	public static FloatTile bestForRegion(double minLat, double minLon, double maxLat, double maxLon, int pixelWidth, int pixelHeight,
										  double multiplier)
	{
		double tilesWide = pixelWidth / (double)Tiles.dim, tilesHigh = pixelHeight / (double)Tiles.dim;
		double lat = (minLat + maxLat) / 2, lon = (minLon + maxLon) / 2;//center of the region
		int zoom = 0;

		if(minLat >= maxLat || minLon >= maxLon)
			return new FloatTile(0, lat, lon);
		multiplier *= 2;

		while(true)
		{
			//how many tiles wide/high the region is at this zoom level
			double regionW = fullTileX(zoom, lat, maxLon) - fullTileX(zoom, lat, minLon);
			double regionH = fullTileY(zoom, minLat, lon) - fullTileY(zoom, maxLat, lon);
			assert(regionW >= 0);
			assert(regionH >= 0);
			if(regionW * multiplier > tilesWide || regionH * multiplier > tilesHigh)
				return new FloatTile(zoom, fullTileX(zoom, lat, lon), fullTileY(zoom, lat, lon));
			zoom++;
		}
	}


	/**A {@link TilePos}, but X and Y are floating-point.*/
	public static class FloatTile
	{
		public final int zoom;
		public final double x, y;
		public FloatTile(int zoom, double x, double y)
		{
			this.zoom = zoom;
			this.x = x;
			this.y = y;
		}
	}


	/**Returns the smallest tile that contains the requested region completely.*/
	public static TilePos tileContaining(double minLat, double minLon, double maxLat, double maxLon)
	{
		if(minLat == maxLat && minLon == maxLon)
			return new TilePos(0, 0, 0);

		int zoom = 0, x = 0, y = 0;
		for(int curZoom = 0; true; curZoom++)
		{
			int x1 = tileX(curZoom, minLat, minLon), x2 = tileX(curZoom, maxLat, maxLon);
			int y1 = tileY(curZoom, minLat, minLon), y2 = tileY(curZoom, maxLat, maxLon);
			if(x1 != x2 || y1 != y2)
				break;

			x = x1;
			y = y1;
			zoom = curZoom;
		}

		return new TilePos(zoom, x, y);
	}



	/**Returns how many degrees of longitude wide each pixel is at a zoom level.*/
	public static double pixelWidthLon(int zoom)
	{
		return 360.0 / (Tiles.maxIndex(zoom) + 1) / Tiles.dim;
	}
	/**Returns how many degrees of latitude high each pixel is at a zoom level.*/
	public static double pixelHeightLat(int zoom){return 180.0 / (Tiles.maxIndex(zoom) + 1) / Tiles.dim;}


	/**Returns the latitude of a tile position.*/
	public static double latFor(int zoom, double tileY)
	{
		return 90.0 - 180.0 * tileY / (SlippyCoreKt.maxTileIndex(zoom) + 1);
	}
	/**Returns the longitude of a tile position.*/
	public static double lonFor(int zoom, double tileX)
	{
		return -180.0 + 360.0 * tileX / (SlippyCoreKt.maxTileIndex(zoom) + 1);
	}
}
