package imp.slippy

import kotlin.math.max
import kotlin.math.min

/**A combined image for a tile, containing data for all layers we have rendered images for. Immutable.
 * This class assumes we're always dealing with a whole image, or a quarter of a whole, or a quarter of a quarter, etc.*/
data class CombinedRenderEntry<Img> (
	val image: Img?,
	/**The position this image will be used to render.*/
	val pos: TilePos,
	/**The position this image was originally rendered at. Different from pos if we're reusing wrong-zoom-level images while waiting for new ones to be produced.*/
	val origin: TilePos,
	/**If only part of the source image should be rendered, this is that part.
	 * Null unless we're reusing an image from the wrong zoom level.*/
	val srcPixels: ImgPixels?,
	/**If this render will only cover part of the tile (multiple images needed to fill the whole tile), this is the part of the tile that is covered.
	 * Null unless we're reusing an image from the wrong zoom level.*/
	val dstPixels: ImgPixels?,
) {
	/**Reuses this entry to fill part of a lower-zoom-level tile.
	 * This function expands the source region we're rendering if possible; otherwise it shrinks the destination we cover.
	 * This function doesn't check whether there's a useful image to reuse.*/
	fun zoomOut(settings: SlippySettings): CombinedRenderEntry<Img> {
		val p = pos.above()
		var sp = srcPixels
		var dp = dstPixels
		if(sp != null) // if we're only showing part of the image we have, show more
			sp = sp.quadruple(settings.dim)
		else // otherwise our image is valid for less of the destination that before
			dp = ImgPixels.quarter(dstPixels, settings.dim, pos.centerX > p.centerX, pos.centerY > p.centerY)
		return CombinedRenderEntry(image, p, origin, sp, dp)
	}

	/**Reuses this entry to fill 4 lower-zoom-level tiles. Each lower entry is provided to the lambda.
	 * If the image we have is able to cover a whole tile at the next zoom level, we'll call the lambda 4 times, once per quarter of the original.
	 * But if the image we have is only able to cover 1 quarter, the lambda will only be called once.
	 * This function doesn't check whether there's a useful image to reuse.*/
	inline fun zoomIn(settings: SlippySettings, consumer: (CombinedRenderEntry<Img>) -> Unit) {
		if(dstPixels == null) { // if we're filling the whole destination currently, we can fill all 4 quarters
			// upper-left
			var sp = ImgPixels.quarter(srcPixels, settings.dim, false, false)
			var p = pos.below(false, false)
			consumer(CombinedRenderEntry(image, p, origin, sp, null))
			// upper-right
			sp = ImgPixels.quarter(srcPixels, settings.dim, true, false)
			p = pos.below(true, false)
			consumer(CombinedRenderEntry(image, p, origin, sp, null))
			// lower-left
			sp = ImgPixels.quarter(srcPixels, settings.dim, false, true)
			p = pos.below(false, true)
			consumer(CombinedRenderEntry(image, p, origin, sp, null))
			// lower-right
			sp = ImgPixels.quarter(srcPixels, settings.dim, true, true)
			p = pos.below(true, true)
			consumer(CombinedRenderEntry(image, p, origin, sp, null))
		} else { // we can't fill the whole destination - figure out which quarter we can
			val right = dstPixels.x0 > 0
			val bottom = dstPixels.y0 > 0
			val p = pos.below(right, bottom)
			val sp = ImgPixels.quarter(srcPixels, settings.dim, right, bottom)
			val dp = dstPixels.quadruple(settings.dim)
			consumer(CombinedRenderEntry(image, p, origin, sp, dp))
		}
	}

	companion object {
		/**Creates an empty entry that doesn't contain an image.*/
		fun <Img> empty(pos: TilePos) = CombinedRenderEntry<Img>(
			image = null,
			pos = pos,
			origin = pos,
			srcPixels = null,
			dstPixels = null,
		)
	}
}

/**A range of pixels in an image. A rectangular region from (x0,y0) to (x1,y1), inclusive.
 * The minimum value for any of the coordinates is zero; the max is 1 less than the width or height of the image.*/
data class ImgPixels (
	val x0: Int,
	val y0: Int,
	val x1: Int,
	val y1: Int,
) {
	val width: Int get() = x1 - x0 + 1
	val height: Int get() = y1 - y0 + 1

	init {
		assert(x0 >= 0 && y0 >= 0 && x1 >= x0 && y1 >= y0) { "Invalid $this" }
	}

	/**Given that this object represents a range of pixels in a dim x dim image, this function returns a larger region of pixels with double the width and height.
	 * If the returned object would cover the whole image, this function returns null.*/
	fun quadruple(dim: Int): ImgPixels? {
		val w = width * 2
		val h = height * 2
		if(w >= dim && h >= dim)
			return null
		val cx = (x0 + x1) / 2
		val cy = (y0 + y1) / 2
		return ImgPixels(
			max(0, cx - w/2),
			max(0, cy - h/2),
			min(dim - 1, cx + w/2),
			min(dim - 1, cy + h/2),
		)
	}

	companion object {
		/**Given an object that represents part of a dim x dim image, this function computes a new region that has half the width and height.
		 * The returned region will be in one of the four corners of the image; the other args determine exactly which corner.
		 * If the input region is null, this is treated as a dim x dim region covering the whole image.*/
		fun quarter(
			pix: ImgPixels?,
			dim: Int,
			/**If true, the returned image will be in the right half of the image, the half with higher X-coordinates.*/
			right: Boolean,
			/**If true, the returned image will be in the bottom half of the image, the half with higher Y-coordinates.*/
			bottom: Boolean,
		): ImgPixels {
			assert(pix == null || (pix.x1 < dim && pix.y1 < dim)) { "$pix doesn't make sense for a dim=$dim image" }
			var x0 = pix?.x0 ?: 0
			var y0 = pix?.y0 ?: 0
			var x1 = pix?.x1 ?: dim - 1
			var y1 = pix?.y1 ?: dim - 1
			val w = (x1 - x0 + 1) / 2
			val h = (y1 - y0 + 1) / 2

			if(right)
				x0 += w
			else
				x1 -= w
			if(bottom)
				y0 += h
			else
				y1 -= h

			return ImgPixels(x0, y0, x1, y1)
		}
	}
}