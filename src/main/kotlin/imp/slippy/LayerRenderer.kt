package imp.slippy

/**An object that can draw map tiles. Images are always square.
 * A slippy map has of 1 or more layers that are drawn on top of each other; all but the first layer should be partially transparent.
 * Must be thread-safe.*/
interface LayerRenderer<Img> {
    /**Renders a tile.
     * If an image argument is provided, this renderer may reuse it.
     * The renderer is always allowed to ignore the provided image and create a new one if it prefers.*/
    suspend fun renderTile(ctx: SlippyRenderCtx, image: Img?): Img
}
