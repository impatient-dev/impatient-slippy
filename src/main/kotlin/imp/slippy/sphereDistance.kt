package imp.slippy

import imp.util.radians
import imp.util.sqrt
import imp.util.squared
import kotlin.math.*


/**Earth radius in meters (average).*/
const val EARTH_RADIUS_M = 6_371_009.0


/**An algorithm that calculates the distance between 2 points on a sphere.
 * This is the "great-circle" distance along the surface of the sphere, and doesn't consider any elevation difference.
 * The result is in the same units as the radius, meters by default.
 * Details: https://en.wikipedia.org/wiki/Great-circle_distance
 *
 * There are 3 implementations, but you should just use the law-of-cosines one.*/
interface SphereDistanceCalculator {
	fun radians(lat1: Double, lon1: Double, lat2: Double, lon2: Double, radius: Double = EARTH_RADIUS_M): Double
	fun degrees(lat1: Double, lon1: Double, lat2: Double, lon2: Double, radius: Double = EARTH_RADIUS_M): Double =
		radians(lat1.radians(), lon1.radians(), lat2.radians(), lon2.radians(), radius)
}


/**Basic calculator. Wikipedia says accuracy is low for distances of a few meters or less; it looks fine to me.*/
object LawCosinesSDC : SphereDistanceCalculator {
	override fun radians(lat1: Double, lon1: Double, lat2: Double, lon2: Double, radius: Double) =
		radius * acos(
			cos(lat1) * cos(lat2) * cos(lon1 - lon2) +
				sin(lat1) * sin(lat2)
		)
	override fun degrees(lat1: Double, lon1: Double, lat2: Double, lon2: Double, radius: Double): Double {
		val lr1 = lat1.radians()
		val lr2 = lat2.radians()
		val lonDiff = (lon2 - lon1).radians()
		return radius * acos(
			cos(lr1) * cos(lr2) * cos(lonDiff) +
				sin(lr1) * sin(lr2)
		)
	}
}


/**Uses the haversine formula.
 * Wikipedia says this is more accurate for small differences (and less accurate for points on opposite sides of the sphere),
 * but to me the results are identical to law of cosines, just more expensive to compute.*/
object HaversineSDC : SphereDistanceCalculator {
	override fun radians(lat1: Double, lon1: Double, lat2: Double, lon2: Double, radius: Double): Double {
		val dlat = lat2 - lat1
		val dlon = lon2 - lon1
		return radius * archav(
			hav(dlat) +
			(1 - hav(dlat) - hav(lat1 + lat2)) * hav(dlon)
		)
	}

	private fun hav(angle: Double) = sin(angle / 2).squared()
	private fun archav(d: Double) = 2 * asin(d.sqrt())
}


/**Uses the Vicenty formula. Wikipedia says this is accurate for all distances, but in my tests this completely breaks for huge distances, returning negative numbers.*/
object VicentySDC : SphereDistanceCalculator {
	override fun radians(lat1: Double, lon1: Double, lat2: Double, lon2: Double, radius: Double): Double {
		val dlon = lon2 - lon1
		val topLeft = (cos(lat2) * sin(dlon)).squared()
		val topRight = (cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dlon)).squared()
		val bottom = sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon)
		return radius * atan(sqrt(topLeft + topRight) / bottom)
	}
}