package imp.slippy

import kotlin.math.roundToInt

/**A vector of length 2, composed of doubles. Immutable.*/
data class Vec2d (val x: Double, val y: Double) {
	override fun toString() = "($x, $y)"

	fun plus(x: Double, y: Double) = Vec2d(this.x + x, this.y + y)
	operator fun plus(vec: Vec2d) = Vec2d(x + vec.x, y + vec.y)

	fun minus(x: Double, y: Double) = Vec2d(this.x - x, this.y - y)
	operator fun minus(vec: Vec2d) = Vec2d(x - vec.x, y - vec.y)

	operator fun times(d: Double) = Vec2d(x * d, y * d)
	operator fun times(i: Int) = Vec2d(x * i, y * i)

	operator fun div(d: Double) = Vec2d(x / d, y / d)

	fun roundToInt() = Vec2i(x.roundToInt(), y.roundToInt())
}




/**A vector of length 2, composed of longs. Immutable.*/
data class Vec2l (val x: Long, val y: Long) {
	override fun toString() = "($x, $y)"

	fun plus(x: Long, y: Long) = Vec2l(this.x + x, this.y + y)
	operator fun plus(vec: Vec2l) = Vec2l(x + vec.x, y + vec.y)

	fun minus(x: Long, y: Long) = Vec2l(this.x - x, this.y - y)
	operator fun minus(vec: Vec2l) = Vec2l(x - vec.x, y - vec.y)

	operator fun times(d: Long) = Vec2l(x * d, y * d)
	operator fun times(i: Int) = Vec2l(x * i, y * i)

	operator fun div(d: Long) = Vec2l(x / d, y / d)
}

/**A vector of length 2, composed of ints. Immutable.*/
data class Vec2i (val x: Int, val y: Int) {
	override fun toString() = "($x, $y)"

	fun plus(x: Int, y: Int) = Vec2i(this.x + x, this.y + y)
	operator fun plus(vec: Vec2i) = Vec2i(x + vec.x, y + vec.y)

	fun minus(x: Int, y: Int) = Vec2i(this.x - x, this.y - y)
	operator fun minus(vec: Vec2i) = Vec2i(x - vec.x, y - vec.y)

	operator fun times(i: Int) = Vec2i(x * i, y * i)

	operator fun div(d: Int) = Vec2i(x / d, y / d)
}