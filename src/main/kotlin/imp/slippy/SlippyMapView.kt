package imp.slippy

import java.lang.Math.floorDiv
import java.lang.Math.floorMod
import javax.annotation.concurrent.Immutable
import kotlin.math.*

@Immutable // not the Compose annotation - Compose isn't available in this project
data class SlippyMapView (
	/**The tile at the center of this view.*/
	val pos: TilePos,
	/**All tiles that are at least partially visible.*/
	val region: TileRegion,
	/**How many pixels to the east of the upper-left of "pos" is the center of the drawn map. Always positive.*/
	val offX: Int,
	/**How many pixels to the south of the upper-left of "pos" is the center of the drawn map. Always positive.*/
	val offY: Int,
	/**How many pixels wide the viewport is.*/
	val w: Int,
	/**How many pixels high the viewport is.*/
	val h: Int,
) {
	inline val zoom get() = pos.zoom

	override fun toString() = "SlippyMapView($pos + ($offX,$offY) pixels, ${w}x$h, $region)"

	/**Returns a new view offset by the provided number of pixels right (east) and down (south).
	 * This function does not change the zoom or viewport width/height.*/
	fun pan(dx: Int, dy: Int, dim: Int): SlippyMapView {
		// determine center tile
		var tx = pos.x + floorDiv(offX + dx, dim)
		var ty = pos.y + floorDiv(offY + dy, dim)
		// determine pixel offsets
		var ox = floorMod(offX + dx, dim)
		var oy = floorMod(offY + dy, dim)
		// stay inside the viewable area of the map
		val mti = maxTileIndex(pos.zoom)
		if(tx < 0) {
			tx = 0
			ox = 0
		} else if(tx > mti) {
			tx = mti
			ox = dim - 1
		}
		if(ty < 0) {
			ty = 0
			oy = 0
		} else if(ty > mti) {
			ty = mti
			oy = dim - 1
		}

		if(tx == pos.x && ty == pos.y && ox == offX && oy == offY)
			return this
		val region = calcRegion(zoom, tx, ty, ox, oy, w, h, dim)
		return SlippyMapView(TilePos(pos.zoom, tx, ty), region, ox, oy, w, h)
	}


	/**Returns the equivalent view for the next zoom level. Returns the initial object if the zoom would be negative.
	 * The viewport width/height do not change.*/
	fun zoomOut(dim: Int): SlippyMapView {
		if(zoom == 0) return this
		val left = pos.x % 2 == 0
		val top = pos.y % 2 == 0
		val above = pos.above()
		val ox = offX / 2 + if(left) 0 else dim / 2
		val oy = offY / 2 + if(top) 0 else dim / 2
		val region = calcRegion(above.zoom, above.x, above.y, ox, oy, w, h, dim)
		return SlippyMapView(above, region, ox, oy, w, h)
	}

	/**Returns the equivalent view for the next zoom level. The viewport width/height do not change.
	 * Returns the initial object if the zoom level would be too high.*/
	fun zoomIn(dim: Int): SlippyMapView {
		if(zoom >= maxZoom)
			return this
		val hdim = dim / 2
		var ox = offX
		var oy = offY
		val right = ox >= hdim
		val down = oy >= hdim
		if(right) ox -= hdim
		if(down) oy -= hdim
		val below = pos.below(right = right, down = down)
		val region = calcRegion(below.zoom, below.x, below.y, ox, oy, w, h, dim)
		return SlippyMapView(below, region, ox, oy, w, h)
	}

	/**Changes the viewport size (and potentially the bounds of the region), but not the center or zoom level.*/
	fun resize(w: Int, h: Int, dim: Int): SlippyMapView {
		val region = calcRegion(zoom, pos.x, pos.y, offX, offY, w, h, dim)
		return SlippyMapView(pos, region, offX, offY, w, h)
	}

	/**Converts a position on-screen, in pixels, to a position on the map (X,Y).
	 * @param dim how many pixels wide/high each rendered tile is*/
	fun pixelPos(x: Double, y: Double, dim: Int): Vec2d {
		// pixel offsets relative to the center of the canvas
		val xc = x - w / 2
		val yc = y - h / 2
		// pixel offsets relative to the northwest of the central tile
		val xse = xc + offX
		val yse = yc + offY
		// tiles
		val xt = pos.x + xse / dim
		val yt = pos.y + yse / dim

		val t = tilesWH(zoom)
		return Vec2d(xt / t, yt / t)
	}


	companion object {
		fun at(
			zoom: Int,
			lat: Double,
			lon: Double,
			projection: MapProjection,
			w: Int,
			h: Int,
			dim: Int,
		): SlippyMapView {
			check(abs(lat) <= projection.maxLat)
			val center = projection.convertll(lat, lon) * tilesWH(zoom)
			val x = floor(center.x)
			val y = floor(center.y)
			val ox = (dim * (center.x - x)).roundToInt()
			val oy = (dim * (center.y - y)).roundToInt()
			val xx = x.roundToInt()
			val yy = y.roundToInt()
			val region = calcRegion(zoom, xx, yy, ox, oy, w, h, dim)
			return SlippyMapView(TilePos(zoom, xx, yy), region, ox, oy, w, h)
		}
	}
}




private fun calcRegion(
	zoom: Int,
	/**X-coordinate of the tile the view is centered around.*/
	x: Int,
	y: Int,
	/**Pixels.*/
	offX: Int,
	offY: Int,
	/**Viewport width in pixels.*/
	w: Int,
	h: Int,
	/**How many pixels wide/high a tile image is.*/
	dim: Int,
	maxTileIndex: Int = maxTileIndex(zoom),
): TileRegion {
	// half width/height, + 1 to avoid rounding down
	val hw = (w / 2) + 1
	val hh = (h / 2) + 1
	val rx0 = max(0, x - ((hw - offX) divRoundUp dim))
	val ry0 = max(0, y - ((hh - offY) divRoundUp dim))
	val rx1 = min(maxTileIndex, x + (hw + offX) / dim)
	val ry1 = min(maxTileIndex, y + (hh + offY) / dim)
	return TileRegion(zoom, rx0, ry0, rx1, ry1)
}


private infix fun Int.divRoundUp(d: Int): Int = this / d + if(this % d == 0) 0 else 1