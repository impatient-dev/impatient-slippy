package imp.slippy

import imp.util.logger

private val log = CombinedRenderHolder::class.logger

/**Holds onto a set of combined-image entries that are needed to render the current map view, and updates this set of entries as the situation changes.
 * This class is responsible for reusing wrong-zoom-level images while we wait for images of the correct zoom level to be rendered.
 * This class only handles combined images for all layers, not images for individual layers.
 * The list of combined entries is immutable - once this class sends it to onChange, we will not modify that list further,
 * instead creating a new list when changes happen.
 * This class is not thread-safe and may only be accessed on the main thread/dispatcher.*/
class CombinedRenderHolder <Img> (
	region: TileRegion,
	private val settings: SlippySettings,
	/**Called every time the set of available images changes, on the main thread/dispatcher.*/
	private val onChange: (List<CombinedRenderEntry<Img>>) -> Unit,
) {
	private var grid = CombinedRenderEntryGrid.create<Img>(region, settings)

	init {
		onChange(grid.entries)
	}


	fun putRender(pos: TilePos, image: Img) {
		val g = grid.copyWithImageIfUsed(pos, image)
		if(g != null) {
			grid = g
			onChange(g.entries)
		}
	}

	/**Changes which entries we'll hold onto for rendering. Try not to call this if the region hasn't changed.*/
	fun setRegion(region: TileRegion) {
		grid = alter(region)
		onChange(grid.entries)
	}

	private fun alter(r: TileRegion): CombinedRenderEntryGrid<Img> {
		log.trace("Alter {}  -->  {}.", this.grid.region, r)
		if(r.zoom != grid.region.zoom) {
			return if(r.zoom == grid.region.zoom + 1)
				grid.toZoomedIn(r)
			else if(r.zoom == grid.region.zoom - 1)
				grid.toZoomedOut(r)
			else
				CombinedRenderEntryGrid.create(r, settings)
		}

		if(!grid.region.overlaps(r))
			return CombinedRenderEntryGrid.create(r, settings)
		val g = grid.copy()

		// shrink
		while(g.region.width > r.width)
			g.deleteCol(g.region.xMin >= r.xMin)
		while(g.region.height > r.height)
			g.deleteRow(g.region.yMin >= r.yMin)

		// pan (after shrink to avoid crossing the world boundary)
		while(g.region.xMin < r.xMin)
			g.moveXUp()
		while(g.region.xMin > r.xMin)
			g.moveXDown()
		while(g.region.yMin < r.yMin)
			g.moveYUp()
		while(g.region.yMin > r.yMin)
			g.moveYDown()

		// expand
		while(g.region.width < r.width)
			g.addCol(true)
		while(g.region.height < r.height)
			g.addRow(true)

		return g
	}
}