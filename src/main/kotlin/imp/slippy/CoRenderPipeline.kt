package imp.slippy

import imp.util.arrayListOfNull
import imp.util.inlineReplaceAll
import imp.util.minusAt
import imp.util.plusAt
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.system.exitProcess


private class RenderTask<Img> (
	val pos: TilePos,
	val layerIndex: Int,
	val renderer: LayerRenderer<Img>,
	/**If true, render even if we already have this image.*/
	val overwrite: Boolean,
)


/**Coroutine-based tile render pipeline.
 * Manages a set of coroutines that render tiles for the layers you request.*/
class CoRenderPipeline <Img, Combined>(
	private val basics: ImageBasics<Img>,
	private val converter: (Img) -> Combined,
	private val settings: SlippySettings,
	/**Called whenever we produce a combined (all-layer) image. There's no guarantee this is called on a particular thread/dispatcher.*/
	private val onProduceCombinedImage: suspend (TilePos, Combined) -> Unit,
	@Volatile private var layers: Array<LayerRenderer<Img>> = emptyArray(),
	private val onError: (Throwable) -> Unit = { exitProcess(-1) }, //TODO
) : AutoCloseable {
	private val toRender = Channel<RenderTask<Img>>(Channel.UNLIMITED)
	private val toCombine = Channel<TilePos>(Channel.UNLIMITED)
	private val stacks = HashMap<TilePos, TileRenderStack2<Img>>()
	/**Must be locked while accessing the stacks map or any stacks in it.*/
	private val mutex = Mutex()
	private val rootJob: Job = launchCoroutines()


	suspend fun addLayer(index: Int, renderer: LayerRenderer<Img>) = mutex.withLock {
		layers = layers.plusAt(index, renderer)
		stacks.inlineReplaceAll { pos, stack ->
			toRender.send(RenderTask(pos, index, renderer, false))
			stack.plusLayer(index)
		}
	}

	suspend fun removeLayer(index: Int) = mutex.withLock {
		layers = layers.minusAt(index)
		stacks.replaceAll { _, stack -> stack.minusLayer(index) }
	}

	suspend fun addRemove(add: TileRegion?, remove: TileRegion?) = mutex.withLock {
		val ll = layers
		remove?.forEachPos { stacks.remove(it) }
		add?.forEachPos { pos ->
			stacks.put(pos, TileRenderStack2(pos, arrayListOfNull(ll.size)))
			for(i in 0 until ll.size)
				toRender.send(RenderTask(pos, i, ll[i], false))
		}
	}

	suspend fun addRemove(add: Collection<TilePos>, remove: Collection<TilePos>) = mutex.withLock {
		val ll = layers
		remove.forEach { stacks.remove(it) }
		add.forEach { pos ->
			stacks.put(pos, TileRenderStack2(pos, arrayListOfNull(ll.size)))
			for(i in 0 until ll.size)
				toRender.send(RenderTask(pos, i, ll[i], false))
		}
	}


	private suspend fun shouldRender(task: RenderTask<Img>): Boolean {
		val ll = layers
		if(task.layerIndex >= ll.size || layers[task.layerIndex] !== task.renderer)
			return false
		mutex.withLock {
			val stack = stacks[task.pos]
			if(stack == null)
				return false
			return task.overwrite || stack.uncombined[task.layerIndex] == null
		}
	}


	private fun launchCoroutines(): Job = CoroutineScope(Dispatchers.Default).launch {
		for(i in 0 until settings.renderThreads)
			launch { coroutineRender(this) }
		launch { coroutineCombine(this) }
	}

	/**A coroutine that continually renders images and puts them into the correct stack.*/
	private suspend fun coroutineRender(scope: CoroutineScope) {
		while(scope.isActive) {
			val task = toRender.receive()
			if(!shouldRender(task))
				continue
			val img = task.renderer.renderTile(SlippyRenderCtx(task.pos, settings.dim, settings.projection), null)
			mutex.withLock {
				if(layerCorrect(task.layerIndex, task.renderer)) {
					val stack = stacks[task.pos]
					if(stack != null) {
						stack.putLayerRender(task.layerIndex, img)
						toCombine.send(task.pos)
					}
				}
			}
		}
	}

	/**A coroutine that constantly combines rendered images into a final, displayable one.*/
	private suspend fun coroutineCombine(scope: CoroutineScope) {
		while(scope.isActive) {
			val pos = toCombine.receive()
			val rendered = mutex.withLock {
				stacks[pos]?.combine(basics, settings)
			}
			if(rendered != null)
				onProduceCombinedImage(pos, converter(rendered))
		}
	}

	override fun close() {
		rootJob.cancel()
	}


	private fun layerCorrect(index: Int, renderer: LayerRenderer<*>): Boolean {
		val a = layers
		return index < a.size && a[index] === renderer
	}
}