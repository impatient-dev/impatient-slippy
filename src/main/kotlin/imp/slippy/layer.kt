package imp.slippy;

import kotlin.math.max

/**An object that can draw on a map.
 * A slippy map consists of 1 or more layers that are drawn on top of each other.
 * Must be thread-safe. */
interface Layer<Img> {
	/**The image class used. */
	val imageClass: Class<Img>
	val defaultRetention: TileRetention

	/**Renders the requested tile.*/
	fun render(pos: TilePos): Img
}




/**Describes which tiles should be kept even when not strictly necessary for rendering.*/
interface TileRetention {
	/**Removes tiles from the store that should no longer be kept, and adds new ones that need to be rendered.
	 * @param region the region that is directly viewable*/
	fun onViewRegionChange(region: TileRegion, layerStore: LayerTileStore<*>)
}


/**A basic tile retention policy defined by a few numbers.*/
data class BasicTileRetention (
	/**How many extra tiles in each direction (horizontal/vertical) should be kept if they have already been rendered.*/
	val keepExtraTileDim: Int,
	/**How many extra zoom levels should be kept if already rendered, above the level that is currently visible.*/
	val keepExtraZooms: Int
) : TileRetention {
	override fun onViewRegionChange(region: TileRegion, layerStore: LayerTileStore<*>) {
		val minZoom = max(0, region.zoom - keepExtraZooms)
		layerStore.clearZoomsOutside(minZoom, region.zoom)

		var r = region
		while(true) {
			layerStore.restrictZoomTo(r)
			layerStore.addRegion(r)
			if(r.zoom > minZoom)
				r = r.above()
			else
				break
		}
	}
}