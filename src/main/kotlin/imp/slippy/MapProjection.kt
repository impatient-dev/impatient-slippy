package imp.slippy

/**Converts between latitude/longitude (in degrees) and (X,Y) position on the map.
 * Map positions range from 0 to 1, where (0,0) is the farthest northwest and (1,1) is the farthest southeast.
 * This means that Y increases to the south, unlike latitude.*/
interface MapProjection {
	/**The maximum latitude displayed by the map, in degrees. If this is 80, the map goes from -80 to +80.*/
	val maxLat: Double
	/**Converts latitude and longitude to (X,Y) position.*/
	fun convert(vec: LatLon): Vec2d
	/**Converts (X,Y) position to latitude and longitude.*/
	fun convert(vec: Vec2d): LatLon
}

inline fun MapProjection.convertll(lat: Double, lon: Double) = convert(LatLon(lat = lat, lon = lon))
inline fun MapProjection.convertxy(x: Double, y: Double) = convert(Vec2d(x = x, y = y))




/**A simple projection where X and Y correspond to longitude and latitude.
 * All possible latitudes (-90 to 90) and longitudes (-180 to 180) are covered.*/
object PlateCarreeProjection : MapProjection {
	override val maxLat = 90.0
	override fun convert(vec: LatLon) = Vec2d(x = vec.lon / 360.0 + 0.5, y = -vec.lat / 180.0 + 0.5)
	override fun convert(vec: Vec2d) = LatLon(lat = (0.5 - vec.y) * 180.0, (vec.x - 0.5) * 360.0)
}


//TODO "web mercator" projection