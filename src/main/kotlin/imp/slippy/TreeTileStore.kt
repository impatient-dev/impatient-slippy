package imp.slippy

///**TODO*/
//class TreeTileStore<T> {
//	/**The root node is never deleted, even if we have no data. All other nodes can be deleted.*/
//	private val root = Node<T>(TilePos(0, 0, 0), null)
//
//
//	/**Stores data at the provided position, replacing any previous data stored at that position.*/
//	@Synchronized fun put(pos: TilePos, data: T) {
//		val lat = pos.centerLat()
//		val lon = pos.centerLon()
//		var node = root
//
//		while(true) {
//			if(node.pos.zoom == pos.zoom) {
//				node.data = data
//				return
//			}
//
//			val up = lat > node.pos.centerLat()
//			val right = lon > node.pos.centerLon()
//			val idx = nodeChildIndex(up, right)
//			val next = node.children[idx]
//			if(next == null) {
//				next = Node(node.pos.below(up, right))
//				node.children[idx] = next
//			}
//			node = next
//		}
//	}
//
//
//	/**TODO*/
//	@Synchronized fun clearExcept(keep: TileRegion, minZoom: Int = 0, maxZoom: Int = defaultMaxZoom) {
//		TODO()
//	}
//
//	/**TODO*/
//	@Synchronized fun getCovering(region: TileRegion) {
//		TODO()
//	}
//}
//
//
//
//
///*Indices in a node's array of children.*/
//private const val INW = 0
//private const val INE = 1
//private const val ISW = 2
//private const val ISE = 3
//
///**Returns an index (INW etc.) corresponding to the provided direction.*/
//private fun nodeChildIndex(up: Boolean, right: Boolean): Int {
//	return if(up) {
//		if(right) INE else INW
//	} else {
//		if(right) ISE else ISW
//	}
//}
//
//
//private class Node<T> (
//	val pos: TilePos,
//	var data: T? = null,
//) {
//	/**TODO
//	 * All children present have some data, either directly or in a direct or indirect child. Nodes are deleted if they contain no data.
//	 * This array is indexed by INW, INE, ISW, ISE, where X increases to the E and Y increases to the N.*/
//	val children: Array<Node<T>?> = Array(4) {null}
//}