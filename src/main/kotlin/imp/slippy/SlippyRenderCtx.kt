package imp.slippy

import kotlin.math.abs


private const val TOO_MANY_TILES_AWAY: Int = 1000

/**Contains the information a renderer needs to know what/how to render.
 * This is also where the math for calculating pixel offsets goes.*/
class SlippyRenderCtx (
	/**The tile to render.*/
	val tile: TilePos,
	/**The width and height of the image.*/
	val dim: Int,
	val projection: MapProjection,
) {
	private val start = tile.nwxy()
	private val tiles = tilesWH(tile.zoom)

	/**Converts a position into a pixel offset from the top-left of this tile.
	 * Returns null if the position is very far from the edges of this tile.*/
	fun pixelOffsets(ll: LatLon): Vec2i? {
		val tileDiff = (projection.convert(ll) - start) * tiles
		if(abs(tileDiff.x) > TOO_MANY_TILES_AWAY || abs(tileDiff.y) > TOO_MANY_TILES_AWAY)
			return null
		return (tileDiff * dim).roundToInt()
	}
}