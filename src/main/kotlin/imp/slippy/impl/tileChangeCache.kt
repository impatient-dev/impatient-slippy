package imp.slippy.impl

import imp.slippy.TileRegion
import imp.slippy.TileStore
import imp.util.logger

/**Tracks the current view region so we don't send changes to the tile store unnecessarily.*/
class TileChangeCache<Img: Any> (val store: TileStore<Img>) {
	private val log = TileChangeCache::class.logger
	private var viewRegion: TileRegion? = null


	/**Notifies this object that the region of viewable tiles may have changed.
	 * If the region is different than the last region this manager received, it will update its tile store and return true.*/
	fun changeRegion(newRegion: TileRegion): Boolean {
		if (this.viewRegion == newRegion)
			return false
		log.debug("Changing region to {}.", newRegion)
		this.viewRegion = newRegion

		synchronized(store) {
			for(layerIndex in 0 until store.layerCount()) {
				val layerStore = store.layerStore(layerIndex)
				val retention = store.layer(layerIndex).defaultRetention
				retention.onViewRegionChange(newRegion, layerStore)
			}
		}

		return true
	}
}
