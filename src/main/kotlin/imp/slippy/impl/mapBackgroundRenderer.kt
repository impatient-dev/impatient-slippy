package imp.slippy.impl

import imp.slippy.TileStore
import imp.util.ReusableNameThreadFactory
import imp.util.logger
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicInteger

/**Manages a pool of threads that render needed tiles/layers.
 * Only accesses a LayerTileStore when it has a lock on it.
 * Thread-safe as long as no one else touches a LayerTileStore without locking it first. */
@Deprecated("")
class MapBackgroundRenderer<Img: Any>(private val tiles: TileStore<Img>, private val maxThreads: Int) {
	private val log = MapBackgroundRenderer::class.logger

	private val runningThreads = AtomicInteger()
	private val threadNamer = ReusableNameThreadFactory("render")


	/**Notifies this renderer that more tiles are available for rendering,
	 * so we should wake up any threads that are sleeping due to lack of work. */
	fun wakeUp() {
		log.debug("Waking up; currently {}/{} threads.", runningThreads.get(), maxThreads)
		val layerCount = tiles.layerCount()
		while (true) {
			val current = runningThreads.get()
			if (current >= maxThreads)
				return
			if (runningThreads.compareAndSet(current, current + 1)) {
				val layer = ThreadLocalRandom.current().nextInt(layerCount)//TODO distribute threads better by remembering which threads are working on which layers
				log.trace("Starting render thread {}/{} for layer {}", current, maxThreads, layer)
				threadNamer.newThread(BackgroundThread(layer, tiles)).start()
			} else
				log.trace("Failed to increment thread counter (contention?); value is {}", runningThreads.get())
		}
	}



	private inner class BackgroundThread (
		val preferredLayerIndex: Int,
		val jobSrc: TileStore<Img>
	) : Runnable {
		override fun run() {
			log.debug("Begin render thread for layer #{}", preferredLayerIndex)
			while (true) {
				//find a job to render
				val job = jobSrc.takeNextJob(preferredLayerIndex)
				log.trace("Next job to render: {}.", job)
				if(job == null) {
					val threads = runningThreads.decrementAndGet()
					log.debug("End of render thread for layer {} ({} threads left)", preferredLayerIndex, threads)
					return
				}

				//render it
				val image = job.layer.render(job.tile)
				log.debug("Successfully rendered {}.", job)
				job.onRender.invoke(image)
			}
		}
	}
}