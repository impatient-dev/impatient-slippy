package imp.slippy

/**Stores tiles for all layers.
 * Thread safety is achieved by locking this object.
 * The provided listener is called every time a tile is rendered.*/
class TileStore <Img: Any> (private val onRender: () -> Unit) {
	private val layers = ArrayList<Layer<Img>>()
	private val layerStores: MutableList<LayerTileStore<Img>> = ArrayList()

	@Synchronized fun layerCount(): Int = layerStores.size

	@Synchronized fun allLayers(): List<Layer<Img>> = ArrayList(layers)
	@Synchronized fun layer(layerIndex: Int): Layer<Img> = layers[layerIndex]
	/**Returns an object that stores tiles for a single layer. You must have a lock on the TileStore when working with a layer tile store.*/
	@Synchronized fun layerStore(layerIndex: Int): LayerTileStore<Img> = layerStores[layerIndex]

	@Synchronized fun addLayer(index: Int, layer: Layer<Img>) {
		layers.add(index, layer)
		layerStores.add(index, LayerTileStore())
	}

	@Synchronized fun removeLayer(index: Int) {
		layers.removeAt(index)
		layerStores.removeAt(index)
		onRender.invoke()
	}


	/**If anything needs rendering, returns a job representing it.
	 * Returns a job for the provided layer unless none are available, in which case it picks another layer.*/
	@Synchronized fun takeNextJob(preferredLayerIndex: Int): RenderJob<Img>? {
		for(c in 0..layerCount()) {
			val layerIndex = (preferredLayerIndex + c) % layerCount()
			val store = layerStores[layerIndex]
			val outPos = store.takeNextToRender()
			if(outPos != null)
				return RenderJob(layerIndex, layers[layerIndex], outPos) {img ->
					synchronized(this) {
						store.putRender(outPos, img)
					}
					onRender.invoke()
				}
		}
		return null
	}
}


/**Specifies a tile that needs rendering in a particular layer, and where the result should be stored.*/
class RenderJob <Img: Any> (
	val layerIndex: Int,
	val layer: Layer<Img>,
	val tile: TilePos,
	val onRender: (img: Img) -> Unit
) {
	override fun toString(): String = "Render[#$layerIndex @ $tile]"
}