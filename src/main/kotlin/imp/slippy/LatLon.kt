package imp.slippy

/**Like Vec2d, but specialized for latitude and longitude, both in degrees. Immutable.*/
data class LatLon (val lat: Double, val lon: Double) {
	override fun toString() = "(lat=$lat, lon=$lon)"

	fun plus(lat: Double, lon: Double) = LatLon(lat = this.lat + lat, lon = this.lon + lon)
	operator fun plus(vec: LatLon) = LatLon(lat = lat + vec.lat, lon = lon + vec.lon)
	fun minus(lat: Double, lon: Double) = LatLon(lat = this.lat - lat, lon = this.lon - lon)
	operator fun minus(vec: LatLon) = LatLon(lat = lat - vec.lat, lon = lon - vec.lon)
	operator fun times(d: Double) = LatLon(lat = lat * d, lon = lon * d)
	operator fun div(d: Double) = LatLon(lat = lat / d, lon = lon / d)
}



/**An axis-aligned bounding box (AABB) specified by minimum and maximum latitude and longitude.
 * Note: minLat must be <= maxLat; minLon therefore corresponds to a larger (or equal) Y-value.*/
data class LatLonBox(
	val minLat: Double,
	val minLon: Double,
	val maxLat: Double,
	val maxLon: Double,
) {
	constructor(min: LatLon, max: LatLon) : this(min.lat, min.lon, max.lat, max.lon)

	init {
		check(minLat <= maxLat && minLon <= maxLon) { "Invalid lat/lon box: min $minLat, $minLon --> max $maxLat, $maxLon" }
	}

	fun min() = LatLon(lat = minLat, lon = minLon)
	fun max() = LatLon(lat = maxLat, lon = maxLon)
	fun upperLeft() = LatLon(lat = maxLat, lon = minLon)
	fun lowerRight() = LatLon(lat = minLat, lon = maxLon)

	fun overlaps(that: LatLonBox): Boolean {
		if(minLat > that.maxLat)
			return false
		if(minLon > that.maxLon)
			return false
		if(maxLat < that.minLat)
			return false
		if(maxLon < that.minLon)
			return false
		return true
	}
}