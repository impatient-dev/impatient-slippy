package imp.slippy

import imp.util.minusAt
import imp.util.plusAt

/**A list of images (1 per layer) for a tile, that need to be combined into a final image for rendering. Not thread-safe.*/
class TileRenderStack2 <Img> ( //TODO rename
	val pos: TilePos,
	val uncombined: ArrayList<Img?>,
) {
	var needsCombine = false
		private set

	override fun toString() = "TileRenderStack($pos count=${uncombined.size})"

	/**Creates a new stack with a null image at the specified position.*/
	fun plusLayer(index: Int) = TileRenderStack2(pos, this.uncombined.plusAt(index, null))
	/**Creates a new stack with an image removed.*/
	fun minusLayer(index: Int) = TileRenderStack2(pos, this.uncombined.minusAt(index))

	fun putLayerRender(layerIndex: Int, image: Img) {
		uncombined[layerIndex] = image
		needsCombine = true
	}

	/**Generates and returns a combined image with data from all layers we have rendered images for, if any.*/
	fun combine(basics: ImageBasics<Img>, settings: SlippySettings): Img? {
		if(!needsCombine)
			return null
		var out: Img? = null
		var outIsWriteable = false

		for(img in uncombined) {
			if(img == null)
				continue
			if(out == null) // 1st image - just use it directly
				out = img
			else if(!outIsWriteable) { // 2nd image - make a new output image and write #1 and #2 to it
				val currentOut = out
				out = basics.newTransparent(settings.dim)
				basics.write(currentOut, out)
				basics.write(img, out)
				outIsWriteable = true
			} else // 3rd or later image: write on top of the others
				basics.write(img, out)
		}

		needsCombine = false
		return out
	}
}

