package imp.slippy

import imp.util.logger

private val log = CombinedRenderEntryGrid::class.logger

/**Make this true to enable full validation that the entries match the region after every change.
 * This is probably kinda expensive and should only be turned on if the grid appears to be working incorrectly.*/
private const val FULL_VALIDATE = false

/**Reuse a higher-zoom tile at a zoom up to this many levels lower.*/
private const val MAX_REUSE_ZOOM_OUT = 8
/**Reuse a lower-zoom tile at a zoom up to this many levels higher.*/
private const val MAX_REUSE_ZOOM_IN = 5

/**A grid of entries filling a particular rectangular region. (A wrapper around a List.)
 * The grid is in a particular order by position; you shouldn't reorder or modify any entries in the list, except via methods in this class.
 * In addition to a grid of entries, the entries list may also include extra entries at the end.
 * These extra entries contain images of the wrong zoom level that fill in parts of the grid/region that would otherwise not have images.
 * These extra entries are removed as rendered images of the correct zoom level come in.*/
class CombinedRenderEntryGrid <Img> (
	val entries: MutableList<CombinedRenderEntry<Img>>,
	val settings: SlippySettings,
	region: TileRegion,
) {
	var region: TileRegion = region
		private set

	init {
		check(entries.size >= region.tiles) { "Region contains ${region.tiles} tiles, but there are only ${entries.size} entries" }
	}

	private inline fun index(x: Int, y: Int) = x + y * region.width
	private inline val firstExtraIndex: Int get() = region.tiles

	fun copy() = CombinedRenderEntryGrid(ArrayList(entries), settings, region)


	/**If the image is outside the region or of the wrong zoom level, returns null.
	 * Otherwise, returns a copy of this grid, with the image placed in the corresponding entry, and any now-obsolete extra entries removed.*/
	fun copyWithImageIfUsed(pos: TilePos, image: Img): CombinedRenderEntryGrid<Img>? {
		if(pos.zoom != region.zoom || !region.contains(pos))
			return null
		log.trace("Copying with image at {}.", pos)
		val i = index(pos.x - region.xMin, pos.y - region.yMin)
		val out = copy()
		val entry = out.entries[i].copy(image = image)
		assert(entry.pos == pos && entry.srcPixels == null && entry.dstPixels == null) { "index $i should have pos $pos and not be wrong-zoom: $entry" }
		out.entries[i] = entry
		out.removeExtrasOverlapping(pos)
		return out
	}

	private fun removeExtrasOverlapping(pos: TilePos) {
		var i = firstExtraIndex
		while(i < entries.size) {
			if(entries[i].pos == pos)
				entries.removeAt(i)
			else
				i++
		}
	}

	/**Removes duplicate extras that fully cover the same position.*/
	private fun deduplicateExtras() {
		val covered = HashSet<TilePos>()
		var i = firstExtraIndex
		while(i < entries.size) {
			val entry = entries[i]
			if(covered.contains(entry.pos)) {
				entries.removeAt(i)
			} else {
				i++
				if(entry.dstPixels == null) // covers the whole tile
					covered.add(entry.pos)
			}
		}
	}


	/**Deletes the first or last column.*/
	fun deleteCol(last: Boolean) {
		val x = if(last) region.width - 1 else 0
		log.trace("Deleting col {}.", x)
		for(y in region.height - 1 downTo 0)
			entries.removeAt(index(x, y))
		region = if(last) region.copy(xMax = region.xMax - 1) else region.copy(xMin = region.xMin + 1)
		maybeValidate()
	}
	/**Adds a column at the start or end.*/
	fun addCol(end: Boolean) {
		val x = if(end) region.width else -1
		val ix = if(end) region.width else 0 // x for choosing what index to insert
		log.trace("Adding col {}.", x)
		for(y in region.height - 1 downTo 0)
			entries.add(index(ix, y), CombinedRenderEntry.empty(TilePos(region.zoom, region.xMin + x, region.yMin + y)))
		region = if(end) region.copy(xMax = region.xMax + 1) else region.copy(xMin = region.xMin - 1)
		maybeValidate()
	}
	/**Deletes the first or last row.*/
	fun deleteRow(last: Boolean) {
		val y = if(last) region.height - 1 else 0
		log.trace("Deleting row {}.", y)
		val i = index(0, y)
		for(x in 0 until region.width)
			entries.removeAt(i)
		region = if(last) region.copy(yMax = region.yMax - 1) else region.copy(yMin = region.yMin + 1)
		maybeValidate()
	}
	/**Adds a row at the start or end.*/
	fun addRow(end: Boolean) {
		val y = if(end) region.height else -1
		val iy = if(end) region.height else 0 // y for choosing what index to insert
		log.trace("Adding row {}.", y)
		for(x in 0 until region.width) {
			entries.add(index(x, iy), CombinedRenderEntry.empty(TilePos(region.zoom, region.xMin + x, region.yMin + y)))
		}
		region = if(end) region.copy(yMax = region.yMax + 1) else region.copy(yMin = region.yMin - 1)
		maybeValidate()
	}

	/**Moves in the +X direction, showing a region with higher X-values than before.*/
	fun moveXUp() {
		deleteCol(false)
		addCol(true)
	}
	/**Moves in the -X direction, showing a region with lower X-values than before.*/
	fun moveXDown() {
		deleteCol(true)
		addCol(false)
	}
	/**Moves in the +Y direction, showing a region with higher Y-values than before.*/
	fun moveYUp() {
		deleteRow(false)
		addRow(true)
	}
	/**Moves in the -Y direction, showing a region with lower Y-values than before.*/
	fun moveYDown() {
		deleteRow(true)
		addRow(false)
	}

	/**Creates a new grid of empty entries, then appends any current entries that are still useful as extras.
	 * The difference in zoom levels must be exactly one.*/
	fun toZoomedOut(r: TileRegion): CombinedRenderEntryGrid<Img> {
		check(r.zoom == region.zoom - 1)
		val out = create<Img>(r, settings, entries.size)
		for(entry in entries)
			out.addHigherZoomIfUseful(entry)
		out.deduplicateExtras()
		return out
	}

	/**Creates a new grid of empty entries, then appends any current entries that are still useful as extras.
	 * The difference in zoom levels must be exactly one.*/
	fun toZoomedIn(r: TileRegion): CombinedRenderEntryGrid<Img> {
		check(r.zoom == region.zoom + 1)
		val out = create<Img>(r, settings, entries.size)
		for(entry in entries)
			out.addLowerZoomIfUseful(entry)
		return out
	}

	/**Takes an entry from the next higher zoom level (more zoomed in) and reuses it at a lower zoom level if possible.*/
	private fun addHigherZoomIfUseful(entry: CombinedRenderEntry<Img>) {
		if(entry.image == null || entry.origin.zoom - region.zoom > MAX_REUSE_ZOOM_OUT)
			return
		assert(entry.pos.zoom == region.zoom + 1)
		val reused = entry.zoomOut(settings)
		if(region.contains(reused.pos))
			entries.add(reused)
	}

	/**Takes an entry from the next lower zoom level (less zoomed in) and reuses it at a higher zoom level if possible.*/
	private fun addLowerZoomIfUseful(entry: CombinedRenderEntry<Img>) {
		if(entry.image == null || region.zoom - entry.origin.zoom > MAX_REUSE_ZOOM_IN)
			return
		assert(entry.pos.zoom == region.zoom - 1)
		entry.zoomIn(settings) { reused ->
			if(region.contains(reused.pos))
				entries.add(reused)
		}
	}

	companion object {
		/**Creates a grid full of entries with no images.*/
		fun <Img>create(
			region: TileRegion,
			settings: SlippySettings,
			/**Extra capacity to reserve at the end of the list, for entries you're planning to add.*/
			extraSpace: Int = 0,
		): CombinedRenderEntryGrid<Img> {
			val out = ArrayList<CombinedRenderEntry<Img>>(region.tiles + extraSpace)
			for(y in 0 until region.height)
				for(x in 0 until region.width)
					out.add(CombinedRenderEntry.empty(TilePos(region.zoom, region.xMin + x, region.yMin + y)))
			return CombinedRenderEntryGrid(out, settings, region).also { it.maybeValidate() }
		}
	}


	/**Validates that all entries have the correct position, if the property is enabled.*/
	private fun maybeValidate() {
		if(!FULL_VALIDATE)
			return
		try {
			for (dx in 0 until region.width) {
				for (dy in 0 until region.height) {
					val pos = TilePos(region.zoom, region.xMin + dx, region.yMin + dy)
					val i = index(dx, dy)
					val entry = entries[i]
					assert(pos == entry.pos) { "CREG: at $i, expected $pos, found ${entry.pos}" }
				}
			}
		} catch(e: AssertionError) {
			println("region: $region (${region.width} x ${region.height})")
			entries.forEachIndexed { i, entry -> println("$i -> $entry") }
			throw e
		}
	}
}