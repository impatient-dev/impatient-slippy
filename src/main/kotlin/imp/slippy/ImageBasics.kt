package imp.slippy

interface ImageBasics <Img> {
	/**Creates an image with the requested width and height in pixels, filled with the color black.*/
	@Deprecated("pointless - use transparent") fun newBlack(dim: Int): Img
	/**Creates a transparent image with the requested width and height in pixels.*/
	fun newTransparent(dim: Int): Img
	/**Writes transparency all over the existing image.*/
	fun writeTransparency(image: Img)
	/**Writes the source onto the destination. If the source has transparent parts, not all of the destination will be overwritten.*/
	fun write(src: Img, dest: Img)
}