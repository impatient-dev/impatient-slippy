package imp.slippy

import java.lang.Integer.max

data class SlippySettings (
	val projection: MapProjection,
	/**Number of pixels wide/high each image is.*/
	val dim: Int,
	val renderThreads: Int,
	val combinerThreads: Int,
) {
	init {
		check(dim > 0 && renderThreads > 0 && combinerThreads > 0)
	}
}

fun defaultSlippySettings(): SlippySettings {
	val threads = Runtime.getRuntime().availableProcessors()
	val combinerThreads = 1
	val renderThreads = max(1, threads - combinerThreads - 1)
	return SlippySettings(PlateCarreeProjection, 256, renderThreads, combinerThreads)
}