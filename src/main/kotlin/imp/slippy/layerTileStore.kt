package imp.slippy

import imp.util.logger
import java.util.*



/**Stores tiles for 1 layer. Not thread-safe, because users want to synchronize once and then do multiple operations.*/
class LayerTileStore <Img: Any> {
	private val log = LayerTileStore::class.logger
	private val zoomTiles: MutableMap<Int, LayerZoomTiles<Img>> = HashMap()
	/**List of zooms that are not fully rendered, in order of priority.*/
	private val zoomsToRender = PriorityQueue<Int>()


	/**Finds the next tile that needs rendering, returns it, and marks it as rendering-in-progress so no one else will try to render it.*/
	fun takeNextToRender(): TilePos? {
		log.trace("Searching for tile to render.")
		while(!zoomsToRender.isEmpty()) {
			val zoom = zoomsToRender.peek()
			val out = zoomTiles[zoom]!!.takeNextToRender()

			if(out != null) {
				log.trace("Found next tile to render: %s", out)
				return out
			} else {
				log.trace("Zoom %s is out of tiles to render.", zoom)
				zoomsToRender.poll()
			}
		}

		log.debug("No tiles are available to render.")
		return null
	}


	/**If the tile at this position has been rendered, returns the image.
	 * If a tile in a higher zoom level has been rendered, uses that as a substitute.
	 * Otherwise, returns null.*/
	fun getRendered(pos: TilePos): Pair<TilePos, Img>? {
		var pos2 = pos
		while(true) {
			val out = zoomTiles[pos2.zoom]?.getRendered(pos2)
			if(out != null)
				return Pair(pos2, out)
			if(pos2.zoom == 0)
				return null
			else
				pos2 = pos2.above()
		}
	}

	/**Provides this store with a rendered tile.*/
	fun putRender(pos: TilePos, img: Img) {
		log.trace("Received render of %s.", pos)
		zoomTiles[pos.zoom]?.putRender(pos, img)
	}


	/**Stops tracking tiles for zoom levels outside an inclusive range.*/
	fun clearZoomsOutside(min: Int, max: Int) {
		val remove = zoomTiles.keys.filter {it < min || it > max}
		log.debug("Clearing zooms outside [%s,%s]: %s", min, max, remove)
		remove.forEach {zoomTiles.remove(it)}
		zoomsToRender.removeAll(remove)
	}

	/**Adds any tiles in the region that are not already being tracked.*/
	fun addRegion(region: TileRegion) {
		log.debug("Adding region (if missing): %s", region)
		zoomTiles.computeIfAbsent(region.zoom) {zoom -> LayerZoomTiles(zoom) }
		if(!zoomsToRender.contains(region.zoom))
			zoomsToRender.add(region.zoom)
		zoomTiles[region.zoom]!!.addMissing(region)
	}

	/**Stops tracking tiles in a zoom level that are outside a rectangular region.*/
	fun restrictZoomTo(region: TileRegion) {
		log.debug("Restricting %s", region)
		zoomTiles[region.zoom]?.removeOutside(region)
	}
}




/**Stores tiles in a layer for one zoom level.*/
private class LayerZoomTiles <Img: Any> (val zoom: Int) {
	private val tiles: MutableMap<TilePos, LayerTile<Img>> = HashMap()

	fun putRender(pos: TilePos, img: Img) {
		tiles[pos]?.img = img
	}

	fun getRendered(pos: TilePos): Img? = tiles[pos]?.img

	fun addMissing(region: TileRegion) {
		region.forEachPos {pos ->
			tiles.computeIfAbsent(pos) { LayerTile() }
		}
	}

	fun removeOutside(region: TileRegion) {
		val it = tiles.iterator()
		while(it.hasNext()) {
			if(!region.contains(it.next().key)) {
				it.remove()
			}
		}
	}

	/**Finds the next tile that needs rendering, if any; marks it rendering-in-progress and returns its position.*/
	fun takeNextToRender(): TilePos? {
		for((pos, tile) in tiles) {
			if(tile.img == null && !tile.isBeingRendered) {
				tile.isBeingRendered = true
				return pos
			}
		}
		return null
	}
}


private data class LayerTile <Img: Any> (
	var img: Img? = null,
	var isBeingRendered: Boolean = false
)