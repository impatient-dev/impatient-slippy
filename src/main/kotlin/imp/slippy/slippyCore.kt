package imp.slippy

import impatience.MathUtil
import kotlin.math.max
import kotlin.math.min

/**The position of a map tile. Immutable.
 * Tiles follow the Google Maps / OSM conventions:
 * -at zoom level 0, the entire world fits on 1 tile; each level quadruples the number of tiles
 * -x ranges from 0 (west) to 2^zoom - 1 (east)
 * -y ranges from 0 (north) to 2^zoom - 1 (south)
 *
 * These objects are compared by the position of their centers.
 * Comparing tiles with different zoom levels is possible but not advised.*/
data class TilePos (val zoom: Int, val x: Int, val y: Int): Comparable<TilePos> {
	init {
		assert(x >= 0)
		assert(x <= maxTileIndex(zoom))
		assert(y >= 0)
		assert(y <= maxTileIndex(zoom))
	}

	override fun toString() = "z$zoom:($x,$y)"

	/**World X coordinate of the center of this tile, where 0 is at the western edge and 1 is at the east.*/
	val centerX: Double get() = (x + 0.5) / tilesWH(zoom)
	/**World Y coordinate of the center of this tile, where 0 is at the northern edge and 1 is at the sloppy.*/
	val centerY: Double get() = (y + 0.5) / tilesWH(zoom)
	val center: Vec2d get() = Vec2d(centerX, centerY)

	/**Coordinates of the northwest corner.*/
	fun nwxy(): Vec2d {
		val t = tilesWH(zoom).toDouble()
		return Vec2d(x / t, y / t)
	}
	/**Coordinates of the northwest corner.*/
	fun nwll(projection: MapProjection): LatLon {
		val t = tilesWH(zoom).toDouble()
		return projection.convertxy(x / t, y / t)
	}

	/**Coordinates of the southeast corner.*/
	fun sell(projection: MapProjection): LatLon {
		val t = tilesWH(zoom).toDouble()
		return projection.convertxy((x + 1) / t, (y + 1) / t)
	}


	/**Returns the tile in the next lower zoom level (more zoomed out) that contains this one.*/
	fun above(): TilePos = if(zoom == 0) throw IllegalStateException("Cannot go above zoom 0") else TilePos(zoom - 1, x / 2, y / 2)
	/**Returns one of the tiles this one contains in the next higher zoom level (more zoomed in).*/
	fun below(right: Boolean, down: Boolean): TilePos = TilePos(zoom + 1, x * 2 + if(right) 1 else 0, y * 2 + if(down) 1 else 0)

	fun plus(x: Int, y: Int): TilePos = TilePos(zoom, x + this.x, y + this.y)
	fun plus(pos: TilePos): TilePos {
		check(zoom == pos.zoom) { "Cannot add zoom ${pos.zoom} to $zoom"}
		return plus(pos.x, pos.y)
	}

	override fun compareTo(other: TilePos): Int {
		var out = centerY.compareTo(other.centerY)
		if(out == 0)
			out = centerX.compareTo(other.centerX)
		return out
	}

	/**Returns true if these are the same position, or if one tile is inside the other (at different zoom levels).*/
	fun overlaps(that: TilePos): Boolean {
		if(this == that)
			return true

		var smaller: TilePos
		val larger: TilePos
		if(this.zoom < that.zoom) {
			smaller = that
			larger = this
		} else {
			smaller = this
			larger = that
		}

		while(smaller.zoom != larger.zoom)
			smaller = smaller.above()
		return smaller.x == larger.x && smaller.y == larger.y
	}
}




/**Because we use ints for tile indexes, it's probably not safe to zoom in farther than this.*/
const val maxZoom = 30
/**Returns how many tiles wide and high the map is at a zoom level.*/
fun tilesWH(zoom: Int): Int = MathUtil.pow2(zoom)
/**Returns the maximum tile index for a zoom level. At this zoom level, the x and y tile coordinates can both be up to this large.*/
fun maxTileIndex(zoom: Int): Int = MathUtil.pow2(zoom) - 1




/**A rectangular selection of tiles at a particular zoom level.*/
data class TileRegion(
	val zoom: Int,
	val xMin: Int,
	val yMin: Int,
	val xMax: Int,
	val yMax: Int
) {
	init {
		assert(xMin <= xMax && xMin >= 0 && xMax <= maxTileIndex(zoom)) { "x: $xMin to $xMax" }
		assert(yMin <= yMax && yMin >= 0 && yMax <= maxTileIndex(zoom)) { "y: $yMin to $yMax" }
	}

	/**How many tiles wide this region is. Always >= 1.*/
	inline val width: Int get() = xMax - xMin + 1
	/**How many tiles tall this region is. Always >= 1.*/
	inline val height: Int get() = yMax - yMin + 1
	/**How many tiles are in this region.*/
	inline val tiles: Int get() = width * height

	inline fun forEachPos(action: (pos: TilePos) -> Unit) {
		for(x in xMin..xMax) {
			for(y in yMin..yMax) {
				action(TilePos(zoom, x, y))
			}
		}
	}

	fun listTiles() = ArrayList<TilePos>(tiles).also { list ->
		forEachPos { pos -> list.add(pos) }
	}

	override fun toString() = "z$zoom:[$xMin,$yMin -- $xMax,$yMax]"

	/**Can only be called for positions of the same zoom level as this region.*/
	fun contains(pos: TilePos): Boolean {
		check(pos.zoom == zoom) { "Cannot check a position with zoom ${pos.zoom} against a region with $zoom"}
		return pos.x >= xMin && pos.y >= yMin && pos.x <= xMax && pos.y <= yMax
	}
	/**Can only be called for regions of the same zoom level.*/
	fun overlaps(that: TileRegion): Boolean = !(xMin > that.xMax || xMax < that.xMin || yMin > that.yMax || yMax < that.yMin)

	/**Returns a region that is expanded by a certain amount in all directions.*/
	fun plusRadius(rad: Int): TileRegion = TileRegion(zoom, xMin - rad, yMin - rad, xMin + rad, yMin + rad)

	/**Returns a region that corresponds to approximately the same area as this one, but in the layer above.*/
	fun above(): TileRegion {
		if(zoom == 0)
			throw IllegalStateException("Cannot go above zoom 0.")
		return TileRegion(zoom - 1, xMin / 2, yMin / 2, xMax / 2, yMax / 2)//Java rounds toward 0
	}

	/**Returns a region that corresponds to the same area, but in the next zoom level down.*/
	fun below(): TileRegion = TileRegion(zoom + 1, xMin * 2, yMin * 2, xMax * 2 + 1, yMax * 2 + 1)

	fun latLon(projection: MapProjection): LatLonBox {
		val t = tilesWH(zoom)
		// y min == lat max
		val min = projection.convertxy(xMin.toDouble() / t, (1 + yMax.toDouble()) / t)
		val max = projection.convertxy((1 + xMax.toDouble()) / t, yMin.toDouble() / t)
		return LatLonBox(min, max)
	}
}


/**Creates a region, truncating X/Y values that are out of bounds for that zoom level.*/
fun clippedRegion(zoom: Int, xMin: Int, yMin: Int, xMax: Int, yMax: Int): TileRegion {
	return TileRegion(zoom, max(0, xMin), max(0, yMin), min(maxTileIndex(zoom), xMax), min(maxTileIndex(zoom), yMax))
}