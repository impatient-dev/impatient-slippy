# impatient-slippy

This is a library for creating a map you can drag and zoom (a "slippy" map).
The map consists of individual tiles that are arranged to fill the viewport.


## TODO

* Consider using the [Web Mercator Projection](https://en.wikipedia.org/wiki/Web_Mercator_projection) (EPSG 3857).

* Consider using [Natural Earth](https://www.naturalearthdata.com/) for basic data about where the continents are.

