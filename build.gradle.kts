plugins {
	id("org.jetbrains.kotlin.jvm")
}

repositories {
	mavenCentral()
}

dependencies {
	api(project(":impatient-coroutines"))
	api(project(":impatient-utils"))

	testImplementation(project(":impatient-junit4"))
	testImplementation(project(":impatient-logback"))
}
